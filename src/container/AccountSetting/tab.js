import React from 'react'
import {UserProfile,Subscription} from '../../container'


const tabNames =[
    {
        label:'User Profile',
        link:'/userProfile',
        value:0,
        component : <UserProfile/>
    },
    {
        label:'Buisness Profile',
        link:'/buisnessProfile',
        value:1,
        component : <Subscription/>
    },
    {
        label:'Subscriptions',
        link:'/subscriptions',
        value:2,
        component : <Subscription/>
    },
    {
        label:'Security',
        link:'/security',
        value:3,
        component : <UserProfile/>
    }

]
export default tabNames