import React, { Component } from 'react'
import Typography from '@material-ui/core/Typography';
import {AccountBody} from '../../component'
import tabData from './tab'


class AccountSetting extends Component {
    constructor(props){
      super(props)
        this.state = {
              value: 0,
            }
    }
      handleChange = (event, value) => {
        this.setState({ value });
      }
      handleChangeIndex = index => {
        this.setState({ value: index });
      }
     TabContainer=(props)=> {
        return (
          <Typography component="div" style={{ padding: 8 * 3 }}>
            {props.children}
          </Typography>
        );
      }

     

    render() {
        return (
          <div className="margin-left margin-right">
                <AccountBody
                  tabData={tabData}
                  handleChangeIndex={this.handleChangeIndex}
                  handleChange={this.handleChange}
                  tabValue={this.state.value}
                  TabContainer={this.TabContainer}
                  
                />
        </div>
        )
    }
}

export default AccountSetting
