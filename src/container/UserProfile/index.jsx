import React, { Component } from 'react'
import {UserProfileComponent} from '../../component'
import { connect } from "react-redux";
import {saveUser} from '../../action'


function getUserName(user){
    return user ? `${user.firstName} ${user.lastName} (${user.role})`: ''
}
function getAvtarTitle(){
    return 'PK'
}

class UserProfile extends Component {
    constructor(props){
        super(props)
        this.state={
            userObj:this.props.user ? Object.assign({},this.props.user) : {},
            isUserForm:false,
            userName:getUserName(this.props.user),
            avtarTitle:getAvtarTitle(),
            emailValidationError:'',
            phoneValidationError:''
        }
    }
    handleBackArrow = (isUserForm) =>{
        this.setState({isUserForm})
    }
    handleInput =(value,key) =>{
        //mutating with the reason , it could be done by other way for that will take time
        this.state.userObj[key]= value 
        this.setState({ userObj: this.state.userObj})
    }
    handleSave =(userObj) =>{
      const emailValidation =  userObj.email.includes('@')
      const phonValidation = (userObj.phoneNumber.length  === 10 )
      if(!emailValidation || !phonValidation){
      this.setState({
          emailValidationError: emailValidation ? '' : 'Invalid email id' ,
        phoneValidationError: phonValidation ?  '' : 'Invalid Phone Number'})
      } else{
          this.props.saveUser(userObj)
      }

    }
    
    render() {
       
        return (
                <UserProfileComponent
                  userObj = {this.state.userObj}
                  isUserForm={this.state.isUserForm}
                  userName={this.state.userName}
                  avtarTitle={this.state.avtarTitle}
                  handleBackArrow={this.handleBackArrow}
                  emailValidationError={this.state.emailValidationError}
                  phoneValidationError={this.state.phoneValidationError}
                  handleSave={this.handleSave}
                  handleInput={this.handleInput}
                />

        )
    }
}

const mapStateToProps = store => ({
    user: store.users.user,
   
});

const mapDispatchToProps = dispatch => ({
    saveUser: user => dispatch(saveUser(user)),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UserProfile)
