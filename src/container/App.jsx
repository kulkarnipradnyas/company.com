import React, { Component } from "react";
import { Route, Switch } from "react-router";
import AccountSetting from "./AccountSetting";
import { Header, Footer } from "../component";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import Login from "../container/Login";

const theme = createMuiTheme();

class App extends Component {
  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <Header />
        <Switch>
          <Route exact path="/" component={Login} />
        </Switch>
        <Footer />
      </MuiThemeProvider>
    );
  }
}

export default App;
