import React from 'react'
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';


const AccountBody = ({ handleChange, tabValue, TabContainer, tabData }) =>  
        <div>
            <AppBar position="static" color="default" >
                <Tabs
                    value={tabValue}
                    onChange={handleChange}
                    indicatorColor="secondary"
                    textColor="primary"
                    fullWidth={false}
                >
                    {tabData.map(({ value, label }) =>         
                        <Tab label={label} value={value} key={value}>
                        </Tab>        
                   )}
                </Tabs>
            </AppBar>

            {
                tabData.map(({ value, component }) => {
                   
                        return tabValue === value ? <TabContainer key={value}>
                            {component}
                        </TabContainer> : null

                })
            }

        </div>
    


export default AccountBody