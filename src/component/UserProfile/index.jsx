import React from 'react'
import Grid from '@material-ui/core/Grid';
import './userProfile.css'
import UserTitleComponent from './UserTitleComponent'
import Input from '@material-ui/core/Input';

const UserProfile = ({ userObj, isUserForm, userName, avtarTitle,handleBackArrow,
    emailValidationError,
    phoneValidationError,handleSave,handleInput }) => (
    <div>
        <UserTitleComponent
            isUserForm ={isUserForm}
            userName={userName}
            avtarTitle ={avtarTitle}
            customTitleClass= "userName margin-name-avtar"
            customAvtarClass="avtar margin-name-avtar"
            handleBackArrow={handleBackArrow}
        /> 
       {isUserForm ? <Grid container spacing={24}>
            <Grid item xs={12} xl={12} >
            <form className="form" onSubmit={e => {e.preventDefault();handleSave(userObj)}}>
                <div>Email</div>
                <Input onChange ={(e)=>handleInput(e.target.value,'email')} value={userObj.email}
                className="inputField" fullWidth={true} placeholder="email" required
                ></Input>
                <div className={emailValidationError ? "errorDiv" : ""}>{emailValidationError}</div>
                <div>First Name</div>
                <Input onChange ={(e)=>handleInput(e.target.value,'firstName')} value={userObj.firstName}
                className="inputField" fullWidth={true} placeholder="first name" required></Input>
                <div>Last Name</div>
                <Input onChange ={(e)=>handleInput(e.target.value,'lastName')} value={userObj.lastName}
                className="inputField" fullWidth={true} placeholder="last name" required></Input>
                <div>Phone Number</div>
                <Input onChange ={(e)=>handleInput(e.target.value,'phoneNumber')} value={userObj.phoneNumber}
                className="inputField" fullWidth={true} type="number" placeholder="phone number" required></Input>
                <div className={phoneValidationError ? "errorDiv" : ""}>{phoneValidationError}</div>
                <button className="submitBtn" type="submit" onSubmit={()=>handleSave()} >Save </button>
            </form> 
            </Grid>
    </Grid>: null}      
    </div>
)
export default UserProfile;