import React from 'react'
import Grid from '@material-ui/core/Grid';
import Avatar from "@material-ui/core/Avatar";
import './userProfile.css'

const userTitleComponent = ({ customTitleClass,
    customAvtarClass,
    userName,
    avtarTitle,
    isUserForm,
    handleBackArrow
 }) => (
    <div>
        <Grid container spacing={24}>
           {isUserForm ? 
           <Grid item xs={1} xl={1}  onClick={()=>handleBackArrow(false)} className ="arrow margin-name-avtar" >
                 {/* <i 
                 className="fal fa-arrow-left arrow margin-name-avtar"
                 onClick={()=>handleBackArrow(false)}
                 />        */}
                 {`<-`}
            </Grid>
            : null}
            <Grid item xs={1} xl={1}>
                <Avatar
                    alt="Adelle Charles"
                    className={customAvtarClass}
                >
                    {avtarTitle}
                </Avatar>
            </Grid>
            <Grid item xs={6} xl={6} className={customTitleClass}>
                <h1>{userName}</h1>
            </Grid>
           {!isUserForm ? <Grid item xs={4} xl={4} className="margin-name-avtar edit">
                <a onClick={()=>handleBackArrow(true)}>Edit</a>
            </Grid>: null}
        </Grid>
        <hr />
    </div>
)
export default userTitleComponent;