import React from 'react'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import './header.css'
import Button from "@material-ui/core/Button";
import Grid from '@material-ui/core/Grid';

const Header = () => (
  <div>
    <Grid container spacing={24} className="margin-left">
      <Grid item xs={5} xl={5}>
        <div className="company-title">
          <span className="company-logo-container"></span>
        </div>
      </Grid>
      <Grid item xs={2} xl={2}>
        <Button
          className="account-selection"
          size="large"
        >My Account</Button>
      </Grid>
    </Grid>
    <Grid>
      <AppBar position="static" color="default" className="account-section-background">
        <Toolbar>
          <Typography variant="h6" color="inherit">
            <div className="account-section-label margin-left">Account Setting</div>
          </Typography>
        </Toolbar>
      </AppBar>
    </Grid>
  </div>)
export default Header