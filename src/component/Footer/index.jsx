import React from 'react'
import Grid from '@material-ui/core/Grid';

const Footer = () =>(
    <div>
           <hr/>
           <div>
          <Grid container spacing={24} className="margin-left">
         
            <Grid item xs={1} xl={1}>
            <span><img src="https://s3-us-west-1.amazonaws.com/companycom-assets/company-footer-logo/footer-logo.png" srcSet="https://s3-us-west-1.amazonaws.com/companycom-assets/company-footer-logo/footer-logo@2x.png 2x, https://s3-us-west-1.amazonaws.com/companycom-assets/company-footer-logo/footer-logo@3x.png 3x" alt="Company.com"/>
            </span>
            </Grid>
            <Grid item xs={3} xl={3}>
            <span>Company.com© 2018 All Rights Reserved</span>
            </Grid>
            <Grid item xs={1} xl={1}>
            <span>Contact Us</span>
            </Grid>
            <Grid item xs={2} xl={2}>
            <span>Terms of Use</span>
            </Grid>
            <Grid item xs={2} xl={2}>
            <span>Privacy Policy</span>
            </Grid>
        </Grid>
        </div>
    </div>
)
export default Footer;