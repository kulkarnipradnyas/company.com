import React from "react";
import StyledTextField from "./StyledTextField";

export default props => {
  <StyledTextField {...props} />;
};
