import styled from "styled-components";
import React from "react";
import TextField from "@material-ui/core/TextField";

const StyledTextField = styled(props => (
  <TextField
    {...props}
    InputLabelProps={{
      ...props.InputLabelProps,
      classes: {
        root: "inputLabelRoot",
        shrink: "inputLabelShrink"
      },
      FormLabelClasses: {
        root: "formLabelRoot",
        error: "formLabelError"
      }
    }}
    InputProps={{
      margin: "none",
      ...props.InputProps,
      classes: {
        root: "inputRoot",
        input: "inputInput",
        underline: "inputUnderline",
        error: "inputError",
        focused: "inputFocused",
        disabled: "inputDisabled"
      }
    }}
    FormHelperTextProps={{
      ...props.FormHelperTextProps,
      classes: {
        root: "formHelperTextRoot",
        error: "formHelperTextError"
      }
    }}
    SelectProps={{
      ...props.SelectProps,
      classes: {
        icon: "selectIcon"
      }
    }}
  />
))`
  && {
    ${props => props.theme.textfield};
  }
  && .inputRoot {
    ${props => props.theme.textfield__inputcontainer};
  }
  && .inputRoot.inputError {
    ${props => props.theme.textfield__inputcontainer__error};
  }
  && .inputInput {
    padding-top: 8px;
    padding-bottom: 8px;
    ${props => props.theme.textfield__input};
  }
  && .inputFocused {
    ${props => props.theme.textfield__input__focused};
  }
  && .inputUnderline:before,
  && .inputUnderline:after,
  &&
    .inputUnderline:hover:not(.inputDisabled):not(.inputFocused):not(.inputError):before {
    ${props => props.theme.textfield__underline};
  }
  && .inputError.inputUnderline:after {
    ${props => props.theme.textfield__underline__error};
  }
  && .inputLabelRoot {
    ${props => props.theme.textfield__label};
  }
  && .inputLabelShrink {
    transform: translate(0, 1.5px);
    transform-origin: top left;
    ${props => props.theme.textfield__label__shrink};
  }
  && .formLabelError {
    ${props => props.theme.textfield__label__error};
  }
  && .formLabelError.inputLabelShrink {
    ${props => props.theme.textfield__label__shrink__error};
  }
  && .formHelperTextRoot {
    ${props => props.theme.textfield__helpertext};
  }
  && .formHelperTextError {
    ${props => props.theme.textfield__helpertext__error};
  }
`;

export default StyledTextField;
