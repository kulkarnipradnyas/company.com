import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './App.css'
import App from './container/App.jsx';
import { Provider } from 'react-redux';
import store from './store'
import { Router } from 'react-router'
import { createBrowserHistory } from 'history'
import 'font-awesome/css/font-awesome.min.css'


ReactDOM.render( 
  <Provider store={store}>
    <Router history={createBrowserHistory()} >
      <App />
    </Router>
  </Provider> ,
document.getElementById('root'));

