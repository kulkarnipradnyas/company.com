import constant from '../constants'
const initialState = {
    user:{
        role:'Admin',
        email:'',
        firstName:'Pradnya',
        lastName:'Kulkarni',
        phoneNumber:'9075089920',
        buisnessName:'Pradnya',
        buisnessAddress: '',
        city:'',
        state:'',
        zipcode: '',
        businessWebsite:'',
        yearsInBusiness:'',
        industry:'',
        numberOfEmployees:''
        

    },
    yearsInBusiness:['less than 1 year','1 to under 2 year', '2 to under 3 year','over 5 year'],
    industry:['1','2','3'],
    numberOfEmployees:['just me','me and 1 other member','2 to 5','6 to 10','11 to 25','over 25 ']
}
function createReducer(initialState, reducerMap) {
    return (state = initialState, action = {}) => {
        const reducer = reducerMap[action.type];

        return reducer
            ? reducer(state, action.payload)
            : state;
    };
}



export default createReducer(initialState,{
    [constant.SET_SEARCH_TEXT](state, payload) {
        return Object.assign({}, state, {
            searchText : payload.searchText,
            searchedCountryArray: payload.filteredArray

        });
    }

})    


